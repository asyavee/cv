# Asya Vishnevskaya
Frontend Engineer (React.js)

email:  hire@asyavee.pro,
        asya.vishn@gmail.com

telegram: [@asyavee](http://t.me/asyavee])

Frontend developer with at least 3 years of expirience in commertial software development. I have a good understanding of user interface design principles and the ability to differentiate between a good interface and an ordinary one. I'm fluent in Git and can easily recover commits after using git reset --hard.
Fluent in English with the ability to read documentation (at least B2 level) and participate in interviews.
Passionate about mentoring and guiding others in their learning journey. Ambitious and aspiring to become a full-stack developer.

## Work Experience

### ZeptoLab
**Frontend Developer**  
Spain, [zeptolab.com](http://zeptolab.com)  
May 2022 – August 2022 (4 months)

- Developed interfaces for internal analytical tools
- Integrated systems for automating enterprise technological and business processes
- Provided IT consulting services

### IT Girl School
**Frontend Program Mentor**  
[itgirlschool.com](http://itgirlschool.com)  
March 2022 – May 2022 (3 months)

- Conducted seminars and workshops
- Checked homework assignments
- Mentored students throughout their learning journey
- Led group projects

### Leroy Merlin
**React.js Frontend Developer**  
Russia, [leroymerlin.ru](http://leroymerlin.ru)  
August 2019 – December 2021 (2 years 5 months)

- Developed internal tools, including:
  - Client application for managing electronic queue systems
  - Client application for fiscal inventory
  - Contributed to the development of in-house solutions, primarily React.js component libraries
- Worked in the retail industry, specializing in DIY and home improvement products

### Freelance
**HTML/CSS Developer**
Moscow  
October 2016 – September 2018 (2 years)

- Created landing page layouts
- Project-based work

## Skills

- Programming languages: HTML, CSS, JavaScript, TypeScript
- Frontend frameworks and libraries: React + Redux, Next.js, Gatsby, Node.js etc.
- Version control: Git
- CMS platforms: 1C-Bitrix, Wordpress
- Project management: Agile methodologies


## Languages

- Russian (native)
- English (fluent)

